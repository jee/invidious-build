#!/bin/bash

if [ ! -d /app ]; then
  echo 'please run with "-v /local/path:/app" like this :'
  echo 'docker run -ti --rm --name invidious_build -v `pwd`/app:/app invidious_comp:latest -e PUID=$UID -e GUID=$GID'
  exit 1
else
  echo 'Clone invidious in /tmp/invidious'
  git clone https://github.com/omarroth/invidious /tmp/invidious
  invidious_commit=$(git --no-pager show -s --format=%h)
  cd /tmp/invidious
  echo ''
  echo 'Invidious will be built with the lastest commit :'
  echo $(git --no-pager --no-pager show -s)
  echo ''
  while true; do
  read -p " Is this ok ? yes or no? " yorn
    case $yorn in
      ([yY][eE][sS] | [yY]) echo 'continu...'
	break;;
      ([nN][oO] | [nN]) echo 'stop...' 
	exit 0;;
      (*) ;;
    esac
  done
  echo ''
  echo 'Update Debian dependencies'
  apt update
  apt upgrade -y
  echo ''
  echo 'Install shard requirements'
  shards update
  shards install
  crystal build --progress src/invidious.cr --release -o /app/invidious_"$(git --no-pager show -s --format=%h)"
  echo ''
  if [ -z "$PUID" ] || [ -z "$GUID" ]; then
    echo "you don't set PUID and/or GUID env binary still owned by root"
    echo ''
  else
    echo "chown $PUID:$GUID invidious_$(git --no-pager show -s --format=%h)"
    chown $PUID:$GUID /app/invidious_"$(git --no-pager show -s --format=%h)"
    echo ''
  fi
  echo 'build ok'
  echo ''
  exit 0
fi
exit 0
