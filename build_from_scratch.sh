#!/bin/bash

echo 'Install Debian dependencies'
apt update
apt upgrade -y
apt install -y -f curl gnupg2 git apt-transport-https
curl -sL "https://keybase.io/crystal/pgp_keys.asc" | apt-key add -
echo "deb https://dist.crystal-lang.org/apt crystal main" | tee /etc/apt/sources.list.d/crystal.list
apt-get update
apt upgrade -f -y
apt install -f -y crystal libssl-dev libxml2-dev libyaml-dev libgmp-dev libreadline-dev librsvg2-dev imagemagick libsqlite3-dev
echo ''
echo 'Clone invidious'
git clone https://github.com/omarroth/invidious $CI_PROJECT_DIR/invidious
cd $CI_PROJECT_DIR/invidious
echo ''
echo 'Invidious will be built with the lastest commit :'
echo $(git --no-pager --no-pager show -s)
echo ''
echo 'Update Debian dependencies'
apt update
apt upgrade -y
echo ''
echo 'Install shard requirements'
shards update
shards install
crystal build --progress src/invidious.cr --release
echo 'build ok with the latest commit :'
echo ''
echo $(git --no-pager --no-pager show -s)
echo ''
echo 'Bye <3'
exit 0

