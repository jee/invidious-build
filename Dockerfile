FROM debian:stretch

RUN apt update && \
    apt upgrade -y && \
    apt install -y -f \ 
    curl \
    gnupg2 \
    apt-transport-https && \
    curl -sL "https://keybase.io/crystal/pgp_keys.asc" | apt-key add - && \
    echo "deb https://dist.crystal-lang.org/apt crystal main" | tee /etc/apt/sources.list.d/crystal.list && \
    apt-get update && \
    apt upgrade -f -y && \
    apt install -f -y \
    crystal \
    libssl-dev \
    libxml2-dev \
    libyaml-dev \
    libgmp-dev \
    libreadline-dev \
    librsvg2-dev \
    imagemagick \
    libsqlite3-dev

COPY ./build.sh /usr/local/bin/build.sh

RUN chmod +x /usr/local/bin/build.sh

ENTRYPOINT /usr/local/bin/build.sh
