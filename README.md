[![pipeline status](https://framagit.org/jee/invidious-build/badges/master/pipeline.svg)](https://framagit.org/jee/invidious-build/commits/master) 

Lastest build 
Binary only: https://framagit.org/jee/invidious-build/-/jobs/artifacts/master/file/invidious/invidious?job=build  
Full Repo  : https://framagit.org/jee/invidious-build/-/jobs/artifacts/master/browse/invidious?job=build  
Each build's logs are available in https://framagit.org/jee/invidious-build/-/jobs/  

**You still have to follow the instructions** except [build part](https://github.com/omarroth/invidious#setup-invidious)

This Build are only tested on Debian Stretch AMD64 it can work on other linux but I don't guarantee it